from config import config
import math
import random
import pygame
from pygame import mixer

pygame.init()
# mixer.init()

configuration = config()
# import player

# adding screen
screen = pygame.display.set_mode((800, 600))

# add background sound
mixer.music.load(configuration.music)
mixer.music.play(-1)

# title and icon
pygame.display.set_caption("GAmE")

# player1
p1Img = pygame.image.load(configuration.cruise)
p1X = 384  # mid
p1Y = 567  # bilkul neeche
p1X_c = 0
p1Y_c = 0


def player1(x, y):
    screen.blit(p1Img, (x, y))  # blit = display


# player2
p2Img = pygame.image.load(configuration.cruise2)
p2X = 384  # mid
p2Y = 0  # bilkul upar
p2X_c = 0
p2Y_c = 0


def player2(x, y):
    screen.blit(p2Img, (x, y))  # blit = display


# i think i don't need gameover flag
# gameover and baari flags
# gameover = 0
baari = 0
# font = pygame.font.Font('freesansbold.ttf', 32)
# def showover(x,y):
#     if baari == 1:
#         test = font.render("bari change", True, (0,0,0))
#         screen.blit(test, (x,y))

# enemy1
n1 = 5
e1Img = []
e1X = []
e1Y = []
e1X_c0 = []
e1X_c1 = []
# e1_state = []

for i in range(5):
    e1Img.append(pygame.image.load(configuration.moving))
    e1X.append(384)  # mid
    e1Y.append(200)
    e1X_c0.append(4)
    e1X_c1.append(4)
    e1X[i] = random.randint(0, 767)
# fix y, randomise x
# fix direction also
e1Y[0] = 56
e1Y[1] = 170
e1Y[2] = 284
e1Y[3] = 398
e1Y[4] = 512


def enemy1(x, y, i):
    screen.blit(e1Img[i], (x, y))  # blit = display


# enemy2
n2 = 14
e2Img = []
e2X = []
e2Y = []
e2X_c = []
e2Y_c = []
# e2_state = []
for i in range(14):
    e2Img.append(pygame.image.load(configuration.fixed))
e2Img[1] = pygame.image.load(configuration.fixed2)
e2Img[2] = pygame.image.load(configuration.fixed2)
e2Img[4] = pygame.image.load(configuration.fixed2)
e2Img[6] = pygame.image.load(configuration.fixed2)
e2Img[7] = pygame.image.load(configuration.fixed2)
e2Img[10] = pygame.image.load(configuration.fixed2)
e2Img[12] = pygame.image.load(configuration.fixed2)
# e2Img[13] = pygame.image.load('fixedwala2.png')

e2X.append(310)  # 1
e2Y.append(0)
e2X.append(500)  # 2
e2Y.append(0)

e2X.append(0)  # 3
e2Y.append(114)
e2X.append(260)  # 4
e2Y.append(114)
e2X.append(767)  # 5
e2Y.append(114)

e2X.append(150)  # 6
e2Y.append(228)
e2X.append(600)  # 7
e2Y.append(228)

e2X.append(245)  # 8
e2Y.append(342)
e2X.append(450)  # 9
e2Y.append(342)

e2X.append(0)  # 10
e2Y.append(456)
e2X.append(376)  # 11
e2Y.append(456)
e2X.append(767)  # 12
e2Y.append(456)

e2X.append(130)  # 13
e2Y.append(567)
e2X.append(550)  # 14
e2Y.append(567)


def enemy2(x, y, i):
    # if e1_state[i] is "alive":
    screen.blit(e2Img[i], (x, y))  # blit = display


def ifCollide(x1, x2, y1, y2):
    # def isCollision(enemyX, enemyY, bulletX, bulletY):
    distance = math.sqrt(math.pow(x1 - x2, 2) + (math.pow(y1 - y2, 2)))
    if distance < 28:
        return True
    return False


# score
score0 = 0
score1 = 0
temp = 0

font = pygame.font.Font('freesansbold.ttf', 20)
def showscore():
    if baari == 0:
        string = font.render("obstacle score p1 = " + str(score0), True, configuration.white)
    if baari == 1:
        string = font.render("obstacle score p2 = " + str(score1), True, configuration.white)
    screen.blit(string, (2, 5))

events = 0
time = 0

font1 = pygame.font.Font('freesansbold.ttf', 10)
def showupdates():
    if events > 1:
        if (events %2) == 0:
            if flag == 0:
                screen.blit(font1.render("player1 won last round", True, configuration.white), (550, 10))
            if flag == 1:
                screen.blit(font1.render("player2 won last round", True, configuration.white), (550, 10))
        else:
            screen.blit(font1.render("player1 scored " + str(score0) + " in the last round", True, (0, 0, 0)), (550, 10))

running = True
while running:
    time += 1
    if baari == 0:
        if p1Y >= 528:
            score0 = 0
        if p1Y < 528:
            score0 = 10
        if p1Y < 456:
            score0 = 25
        if p1Y < 414:
            score0 = 35
        if p1Y < 342:
            score0 = 45
        if p1Y < 300:
            score0 = 55
        if p1Y < 228:
            score0 = 65
        if p1Y < 186:
            score0 = 75
        if p1Y < 114:
            score0 = 90
        if p1Y < 72:
            score0 = 100
    if baari == 1:
        if p2Y <= 72:
            score1 = 0
        if p2Y > 72:
            score1 = 10
        if p2Y > 114:
            score1 = 25
        if p2Y > 186:
            score1 = 35
        if p2Y > 228:
            score1 = 45
        if p2Y > 300:
            score1 = 55
        if p2Y > 342:
            score1 = 65
        if p2Y > 414:
            score1 = 75
        if p2Y > 486:
            score1 = 90
        if p2Y > 528:
            score1 = 100

    # background
    screen.fill(configuration.back)

    # partitions
    pygame.draw.rect(screen, configuration.color, (0, 0, 800, 30))
    pygame.draw.rect(screen, configuration.color, (0, 114, 800, 30))
    pygame.draw.rect(screen, configuration.color, (0, 228, 800, 30))
    pygame.draw.rect(screen, configuration.color, (0, 342, 800, 30))
    pygame.draw.rect(screen, configuration.color, (0, 456, 800, 30))
    pygame.draw.rect(screen, configuration.color, (0, 570, 800, 30))

    #  cross
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        # p1
        if baari == 0:

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    p1X_c = -2
                if event.key == pygame.K_RIGHT:
                    p1X_c = 2
                if event.key == pygame.K_UP:
                    p1Y_c = -2
                if event.key == pygame.K_DOWN:
                    p1Y_c = 2
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or pygame.K_RIGHT:
                    p1X_c = 0
                if event.key == pygame.K_UP or pygame.K_DOWN:
                    p1Y_c = 0
        # p2
        if baari == 1:
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_a or pygame.K_d:
                    p2X_c = 0
                if event.key == pygame.K_w or pygame.K_s:
                    p2Y_c = 0
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    p2X_c = -2
                if event.key == pygame.K_d:
                    p2X_c = 2
                if event.key == pygame.K_w:
                    p2Y_c = -2
                if event.key == pygame.K_s:
                    p2Y_c = 2

    # movement generic
    p1X += p1X_c
    p1Y += p1Y_c
    p2X += p2X_c
    p2Y += p2Y_c

    # speed of enemies
    for i in range(5):
        if baari == 0:
            e1X[i] += e1X_c0[i]
        if baari == 1:
            e1X[i] += e1X_c1[i]

    # boundary
    if p1X <= 0 or p1X >= 768:
        p1X_c = 0
    if p1Y <= 0 or p1Y >= 568:
        p1Y_c = 0
    if p2X <= 0 or p2X >= 768:
        p1X_c = 0
    if p2Y <= 0 or p2Y >= 568:
        p2Y_c = 0

    for i in range(5):  # enemy 1 mechanics and collision
        enemy1(e1X[i], e1Y[i], i)  # after screen.fill
        if e1X[i] <= 0 or e1X[i] >= 768:
            e1X_c0[i] = -e1X_c0[i]
            e1X_c1[i] = -e1X_c1[i]

        # collision
        collision1 = ifCollide(p1X, e1X[i], p1Y, e1Y[i])
        if collision1:
            # sound = mixer.music('looser.mp3')
            # sound.play()
            # e1_state[i] = "dead"

            # do i reallly need gameover flag?
            # gameover = 1
            events += 1
            score0 -= (time/10)
            time = 0
            temp = score0
            baari = not baari
            p1X = 384  # mid
            p1Y = 567  # bilkul upar
            p1X_c = 0
            p1Y_c = 0
        collision2 = ifCollide(p2X, e1X[i], p2Y, e1Y[i])
        if collision2:
            # sound = mixer.Sound('looser.mp3')
            # sound.play()
            # e1_state[i] = "dead"
            # gameover = 1
            events += 1
            score1 -= (time/10)
            time = 0
            if score0 > score1:
                flag = 0
            else:
                flag = 1
            baari = not baari
            p2X = 384  # mid
            p2Y = 0  # bilkul upar
            p2X_c = 0
            p2Y_c = 0

    for i in range(14):  # enemy 2 collision
        enemy2(e2X[i], e2Y[i], i)  # after screen.fill
        collision1 = ifCollide(p1X, e2X[i], p1Y, e2Y[i])
        if collision1:
            # sound = mixer.Sound('looser.mp3')
            # sound.play()
            # gameover = 1
            events += 1
            score0 -= (time/10)
            temp = score0
            time = 0
            baari = not baari
            p1X = 384  # mid
            p1Y = 567  # bilkul neeche
            p1X_c = 0
            p1Y_c = 0
        collision2 = ifCollide(p2X, e2X[i], p2Y, e2Y[i])
        if collision2:
            # sound = mixer.Sound('looser.mp3')
            # sound.play()
            # gameover = 1
            events += 1
            score1 -= (time/10)
            time = 0
            if score0 > score1:
                flag = 0
            else:
                flag = 1
            baari = not baari
            p2X = 384  # mid
            p2Y = 0  # bilkul upar
            p2X_c = 0
            p2Y_c = 0

    # win
    if p1Y < 30:
        events += 1
        score0 -= (time/10)
        time = 0
        temp = score0
        # flag = 0
        baari = 1  # switch turns
        p1X = 384  # mid
        p1Y = 567  # bilkul neeche
        p1X_c = 0
        p1Y_c = 0
        for i in range(5):
            if e1X_c0[i] > 0:
                e1X_c0[i] += 1
            else:
                e1X_c0[i] -= 1
    if p2Y > 540:
        events += 1
        score1 -= (time/10)
        time = 0
        if score0 > score1:
            flag = 0
        else:
            flag = 1
        baari = 0
        p2X = 384  # mid
        p2Y = 0  # bilkul upar
        p2X_c = 0
        p2Y_c = 0
        for i in range(5):
            if e1X_c1[i] > 0:
                e1X_c1[i] += 1
            else:
                e1X_c1[i] -= 1

    # player.player1(p1X,p1Y) #after screen.fill
    player1(p1X, p1Y)  # after screen.fill
    player2(p2X, p2Y)
    # showover(10,10)
    showscore()
    showupdates()
    pygame.display.update()
