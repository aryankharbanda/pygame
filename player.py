import pygame
pygame.init()

import main

global p1Img
global p1X
global p1Y
global p1X_c
global p1Y_c


# player1
p1Img = pygame.image.load('cruise.png')
p1X = 384 #mid
p1Y = 480
p1X_c = 0
p1Y_c = 0
def player1(x,y):
    screen.blit(p1Img, (x,y)) #blit = display

running = True
while running:

    for event in main.pygame.event.get():

        # movement
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                p1X_c = -2
            if event.key == pygame.K_RIGHT:
                p1X_c = 2
            if event.key == pygame.K_UP:
                p1Y_c = -2
            if event.key == pygame.K_DOWN:
                p1Y_c = 2
        # make movement smooth
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or pygame.K_RIGHT:
                p1X_c = 0
            if event.key == pygame.K_UP or pygame.K_DOWN:
                p1Y_c = 0

    # movement
    p1X += p1X_c
    p1Y += p1Y_c

    if p1X <= 0 or p1X >= 768:
        p1X_c = 0
    if p1Y <= 0 or p1Y >= 568:
        p1Y_c = 0