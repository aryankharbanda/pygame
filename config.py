import pygame

class config(object):
	def __init__(self):
		self.music = "bassloop.wav"
		self.white = (0,0,0)
		self.back = (128,0,0)
		self.color = (255, 255, 255)
		self.cruise = "cruise.png"
		self.cruise2 = "cruise2.png"
		self.moving = "movingwala.png"
		self.fixed = "fixedwala.png"
		self.fixed2 = "fixedwala2.png"
